<?php

/**
 * @file
 * Hooks provided by the Commerce Refund Order Item module.
 */

/**
 * This doc it is only as an example of how to use the hook
 *
 * @param $order
 * @param $status
 */
function hook_commerce_refund_order_item_refund_success($order, $status) {
}
