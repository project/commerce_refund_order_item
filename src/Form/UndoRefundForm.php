<?php

namespace Drupal\commerce_refund_order_item\Form;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_price\Price;
use Drupal\commerce_refund_order_item\RefundFormTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UndoRefundForm.
 */
class UndoRefundForm extends FormBase {

  use RefundFormTrait;

  /**
   * Drupal\commerce_refund_order_item\Service\RefundService definition.
   *
   * @var \Drupal\commerce_refund_order_item\Service\RefundService
   */
  protected $commerceRefundOrderItemRefund;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->commerceRefundOrderItemRefund = $container->get('commerce_refund_order_item.refund');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'undo_refund_form';
  }

  public function getPayment() {
    $payment      = Payment::load(\Drupal::routeMatch()->getParameter('commerce_payment'));
    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['#success_message'] = t('Refund undone.');

    $this->addRefundTable($form, $this->getPayment(), TRUE);

    $willDo = [
      'remove any refund information for the above refunded items',
      'add back the amount paid as calculated or entered here'
    ];
    $wontDo = [
      'cancel or void the refund in the payment processor <b>(you must do this manually)</b>',
    ];

    $this->addWillDoWontDo($form, $willDo, $wontDo, $this->t('Note that undoing the refund will:'), $this->t('Undoing the refund will not:'));

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm undo'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values        = $form_state->getValue($form['#parents']);
    $itemsSelected = array_filter($values['table']);

    $itemsWithQuantity = $this->submitGetItems($itemsSelected, 'order_items');
    $adjustments = $this->submitGetItems($itemsSelected, 'adjustments');
    $amount = Price::fromArray($values['amount']);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getPayment();
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway();

    $reflectionClass = new ReflectionClass(get_class($payment_gateway_plugin));
    if (!$reflectionClass->hasMethod('undoRefundPayment')) {
      \Drupal::logger('commerce_refund_order_item')->alert('Payment gateway does not implement refundPayment method (' . $payment_gateway_plugin->getPluginId() . ')');
      $this->commerceRefundOrderItemRefund->undoRefundPaymentNoGateway($payment, $amount, $itemsWithQuantity, $adjustments);
    } else {
      $payment_gateway_plugin->undoRefundPayment($payment, $amount, $itemsWithQuantity, $adjustments);
    }
  }
}
