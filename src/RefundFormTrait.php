<?php

namespace Drupal\commerce_refund_order_item;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Form building for refunds.
 */
trait RefundFormTrait {

  public function addRefundTable(array &$form, Payment $payment, $undoRefund = FALSE) {
    $restrictByPermission = FALSE;
    $restricted = FALSE;

    $account = \Drupal::currentUser();

    $order = $payment->getOrder();
    $orderItems = $order->getItems();
    $options    = [];

    $form['amount']           = [
      '#type'                 => 'commerce_price',
      '#title'                => t('Amount'),
      '#default_value'        => $payment->getBalance()->toArray(),
      '#required'             => TRUE,
      '#disabled'             => !$account->hasPermission('refund arbitrary amount'),
      '#available_currencies' => [$payment->getAmount()->getCurrencyCode()],
    ];

    /**
     * @var \Drupal\commerce_order\Entity\OrderItem $orderItem
     */
    foreach ($orderItems as $orderItem) {
      $quantity = (int) $orderItem->getQuantity();
      $price    = $orderItem->getUnitPrice()->getNumber();
      $amount   = number_format((float) $price, 2, '.', '');
      $currency = $orderItem->getUnitPrice()->getCurrencyCode();
      $x        = 0;

      $hasPermission = $account->hasPermission($orderItem->getPurchasedEntity()->getProduct()->bundle() . ' refund payments');
      if ($restrictByPermission && !$hasPermission) {
        $restricted = TRUE;
        continue;
      }

      if ($orderItem->refunded->value == 1 && !$undoRefund) {
        continue;
      }

      if ($orderItem->refunded->value == 0 && $undoRefund) {
        continue;
      }

      if ($orderItem->refunded->value == 1) {
        $quantity = (int) $orderItem->refunded_quantity->value;
      } else {
        $quantity = (int) $orderItem->getQuantity();
      }

      while ($x < $quantity) {
        $option = [
          'type'        => 'commerce_order_item',
          'id'          => $orderItem->id(),
          'product'     => $orderItem->getTitle(),
          'amount'      => $amount,
          'currency'    => $currency,
          '#attributes' => ['price' => $amount],
          '#disabled'   => !$hasPermission,
        ];
        $options['order_item-' . $orderItem->id() . '-' . $x] = $option;
        $x++;
      }
    }

    $adjustments = $order->collectAdjustments();
    foreach ($adjustments as $adjustment) {
      $title = FALSE;
      $quantity = 1;
      $price    = $adjustment->getAmount()->getNumber();
      $amount   = number_format((float) $price, 2, '.', '');
      $currency = $adjustment->getAmount()->getCurrencyCode();
      $x        = 0;

      $hasPermission = $adjustment->getType() == 'shipping' ? $account->hasPermission('shipping refund adjustment payments') : TRUE;
      if ($restrictByPermission && !$hasPermission) {
        $restricted = TRUE;
        continue;
      }

      while ($x < $quantity) {
        $options[$adjustment->getType() . '-' . $x . '-' . $amount] = [
          'type'        => $adjustment->getType(),
          'id'          => $adjustment->getLabel() . $x,
          'product'     => $adjustment->getLabel(),
          'amount'      => $amount,
          'currency'    => $currency,
          '#attributes' => ['price' => $amount],
          '#disabled'   => !$hasPermission,
        ];
        $x++;
      }
    }

    $header = [
      'id'       => t('ID'),
      'product'  => t('Product'),
      'amount'   => t('Price'),
      'currency' => t('Currency'),
    ];

    $form['table'] = [
      '#type'    => 'tableselect',
      '#header'  => $header,
      '#options' => $options,
      '#empty'   => t('No items found'),
      '#weight'  => -10,
    ];

    $form['#attached']['library'][] = 'commerce_refund_order_item/refund';


    if ($restricted) {
      $form['notes_restricted'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . t('Note some products may be hidden because you do not have permission to refund.') . '</p>',
        '#weight' => 25,
      ];
    }
  }

  public function addWillDoWontDo(array &$form, array $willDo, array $wontDo, TranslatableMarkup $willDoPrefix, TranslatableMarkup $wontDoPrefix) {
    $form['notes'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $willDoPrefix . '<ul><li>' . implode('</li><li>', $willDo) . '</li></ul>' . '</p>',
    ];

    $form['wont_do'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $wontDoPrefix . '<ul><li>' . implode('</li><li>', $wontDo) . '</li></ul>' . '</p>',
    ];
  }

  public function submitGetItems(array $itemsSelected, $typeToReturn = 'order_items') {
    $itemsWithQuantity = [];
    $adjustments = [];
    foreach ($itemsSelected as $item) {
      $ids = explode('-', $item);
      $type = $ids[0];
      $key = $ids[1];
      if ($type == 'order_item') {
        $itemsWithQuantity[$key] = isset($itemsWithQuantity[$key]) ? $itemsWithQuantity[$key] + 1 : 1;
      } else {
        $amount = $ids[2];
        $adjustments[$key] = [$type, $amount];
      }
    }

    if ($typeToReturn == 'adjustments') {
      return $adjustments;
    } else {
      return $itemsWithQuantity;
    }
  }
}
