<?php

namespace Drupal\commerce_refund_order_item\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event fired when an order is partially or wholly refunded
 */
class OrderRefundedEvent extends Event {
  /**
   * The order
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  public $order;

  /**
   * The itemsWithQuantity array,
   * key is order item ID
   * value is the quantity being refunded for that order item
   *
   * @var array
   */
  public $itemsWithQuantity;

  /**
   * Constructs the object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(OrderInterface $order, array $itemsWithQuantity) {
    $this->order = $order;
    $this->itemsWithQuantity = $itemsWithQuantity;
  }

  /**
   * Method to get the record.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Method to get the order items with quantity.
   */
  public function getItemsWithQuantity() {
    return $this->itemsWithQuantity;
  }
}
