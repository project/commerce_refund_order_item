<?php

namespace Drupal\commerce_refund_order_item\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentRefundForm as PaymentRefundFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_refund_order_item\RefundFormTrait;
use ReflectionClass;

class PaymentRefundForm extends PaymentRefundFormBase {

  use RefundFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\commerce_order\Entity\Order $order
     */
    $order      = \Drupal::routeMatch()->getParameter('commerce_order');

    $form['#success_message'] = t('Payment refunded.');

    $this->addRefundTable($form, $this->entity);

    $refundService = \Drupal::service('commerce_refund_order_item.refund');

    // @todo work out how to check if gateway does refund it.
    $refundByGateway = TRUE;
    $restoreStock = $refundService->stockControlledByCoreEvents() || $refundService->stockControlledByRefund();
    $shippingOrder = $refundService->getShipmentsForOrder($order) ? TRUE : FALSE;

    $willDo = [
      'reduce the order total price by that amount',
      'add an order item showing what was refunded',
    ];

    $wontDo = [
      'contact the purchaser',
    ];

    if ($refundByGateway) {
      $willDo[] = 'immediately refund the money to the user via the payment gateway';
    } else {
      $wontDo[] = 'refund the money to the user via the payment gateway';
    }

    if ($restoreStock) {
      $willDo[] = 'restore the stock for the item(s) refunded';
    } else {
      $wontDo[] = 'restore the stock for the item(s) refunded';
    }

    if ($shippingOrder) {
      $willDo[] = '(for shipping orders) remove the quantity from any shipments in draft or processing state';
    }

    $this->addWillDoWontDo($form, $willDo, $wontDo, $this->t('Refunding this order will:'), $this->t('This refund will not:'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $balance = $payment->getBalance();
    if ($amount->greaterThan($balance)) {
      $form_state->setError($form['amount'], t("Can't refund more than @amount.", ['@amount' => $balance->__toString()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    $values        = $form_state->getValue($form['#parents']);

    $itemsSelected = array_filter($values['table']);

    $itemsWithQuantity = $this->submitGetItems($itemsSelected, 'order_items');
    $adjustments = $this->submitGetItems($itemsSelected, 'adjustments');
    $amount = Price::fromArray($values['amount']);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $reflectionClass = new ReflectionClass(get_class($payment_gateway_plugin));
    if (!$reflectionClass->hasMethod('refundPayment')) {
      \Drupal::logger('commerce_refund_order_item')->alert('Payment gateway does not implement refundPayment method (' . $payment_gateway_plugin->getPluginId() . ')');
      $this->commerceRefundOrderItemRefund->refundPaymentNoGateway($payment, $amount, $itemsWithQuantity, $adjustments);
    } else {
      $payment_gateway_plugin->refundPayment($payment, $amount, $itemsWithQuantity, $adjustments);
    }
  }
}
