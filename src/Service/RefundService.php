<?php

namespace Drupal\commerce_refund_order_item\Service;

use Drupal\commerce\Context;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_refund_order_item\Event\OrderRefundedEvent;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Implements Class RefundService.
 */
class RefundService {

  public function refundFromGateway(array $itemsWithQuantity, Price $amount, Payment $payment, array $adjustments) {
    $order = $payment->getOrder();
    $this->registerRefundOrderItem($order, $itemsWithQuantity, $amount);
    $this->registerRefundAdjustments($order, $adjustments, $amount);
    $this->updatePaymentForRefund($payment, $amount);
    $this->dispatchHooksAndEvents($order, $itemsWithQuantity);
  }

  public function undoRefundFromGateway(array $itemsWithQuantity, Price $amount, Payment $payment, array $adjustments) {
    $order = $payment->getOrder();
    $this->registerRefundOrderItem($order, $itemsWithQuantity, $amount, TRUE);
    $this->registerRefundAdjustments($order, $adjustments, $amount, TRUE);

    $amount = new Price(-1 * $amount->getNumber(), $amount->getCurrencyCode());
    $this->updatePaymentForRefund($payment, $amount);

    $this->dispatchHooksAndEvents($order, $itemsWithQuantity, TRUE);
  }

  /**
   * @param $order
   * @param $itemsWithQuantity array keyed with orderItem ID, value is the quantity being refunded
   * @param $amount
   * @throws EntityStorageException
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function registerRefundOrderItem(Order $order, array $itemsWithQuantity, Price $amount, bool $undo = FALSE) {
    foreach ($itemsWithQuantity as $item => $refundedQuantity) {

      $orderItemOriginal = OrderItem::load($item);
      if ($undo) {
        $remainingQuantity = $orderItemOriginal->getQuantity() + $refundedQuantity;
      } else {
        $remainingQuantity = $orderItemOriginal->getQuantity() - $refundedQuantity;
      }

      // If there is remaining quantity in the order item
      // duplicate the order item so we can track the refunded quantity separately
      if (!$undo && $remainingQuantity > 0) {
        $orderItemRefund = $orderItemOriginal->createDuplicate();
        $orderItemOriginal->setQuantity($remainingQuantity);

        $refundedAmountForOrderItem = $orderItemOriginal->getAdjustedUnitPrice()->getNumber() * $refundedQuantity;
        $refundedAmountForOrderItem = max($refundedAmountForOrderItem, $orderItemRefund->getAdjustedTotalPrice()->getNumber());
        $refundedAmountForOrderItem = new Price($refundedAmountForOrderItem, $amount->getCurrencyCode());

        // We set these so we know that THIS order item lost items as part of the refund -
        // However because field refunded is not TRUE, and the actual quantity is > 0,
        // we know the actual quantity is still a sale
        $orderItemOriginal->set('refunded_amount', $orderItemOriginal->refunded_amount->value + $amount);
        $orderItemOriginal->set('refunded_quantity', $orderItemOriginal->refunded_quantity->value + $refundedQuantity);

        $orderItemOriginal->save();
      } else {
        $orderItemRefund = $orderItemOriginal;
        $refundedAmountForOrderItem = $orderItemRefund->getAdjustedTotalPrice();
      }

      $now = DrupalDateTime::createFromTimestamp(time());

      if ($undo) {
        $orderItemRefund->set('refunded', FALSE);
        $orderItemRefund->set('refunded_amount', 0);
        $orderItemRefund->set('refunded_quantity', 0);
        $orderItemRefund->set('refunded_date', NULL);
        $orderItemRefund->setQuantity($refundedQuantity);
      } else {
        $orderItemRefund->set('refunded', TRUE);
        $orderItemRefund->set('refunded_amount', $refundedAmountForOrderItem);
        $orderItemRefund->set('refunded_quantity', $refundedQuantity);
        $orderItemRefund->set('refunded_date', $now->format('Y-m-d\TH:i:s'));
        $orderItemRefund->setQuantity(0);
      }
      $orderItemRefund->save();

      $order->addItem($orderItemRefund);
      $order->save();

      // Restore stock for this:
      $purchasedEntity = $orderItemOriginal->getPurchasedEntity();
      if ($purchasedEntity) {
        $this->restoreStock($purchasedEntity, $refundedQuantity, $orderItemOriginal);
      }

      $this->doLogMessageOrderItem($order, $orderItemOriginal, $refundedQuantity, $remainingQuantity, $undo);
    }

    // Update shipments to avoid shipping refunded items
    $this->updateShipments($order, $itemsWithQuantity);

    $order->recalculateTotalPrice();
    $order->save();
  }

  /**
   * Creates cancelling out adjustments for any refunded adjustments
   *
   * @param $order
   * @param $adjustments array keyed with number, values type then amount
   * @param $amount
   * @throws EntityStorageException
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function registerRefundAdjustments(Order $order, $adjustments, $amount, bool $undo = FALSE) {
    $existingAdjustments = $order->collectAdjustments();

    foreach ($existingAdjustments as $existingAdjustment) {
      foreach ($adjustments as $number => $data) {
        $type = $data[0];
        $amount = $data[1];
        if ($amount == $existingAdjustment->getAmount()->getNumber() && $type == $existingAdjustment->getType()) {
          $newAdjustment = $existingAdjustment->toArray();
          $newAdjustment['label'] = 'REFUNDED ' . $newAdjustment['label'];
          $newAdjustment['amount'] = new Price(-1 * $amount, $newAdjustment['amount']->getCurrencyCode());
          $order->addAdjustment(new Adjustment($newAdjustment));

          $this->doLogMessageAdjustment($order, $existingAdjustment);

          unset($adjustments[$number]);
        }
      }
    }

    $order->save();
  }

  public function doLogMessageOrderItem($order, $orderItemOriginal, $refundedQuantity, $remainingQuantity, $undo = FALSE) {
    /** @var CommerceLogServiceProvider $commerceLog * */
    $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');

    $itemTitle = $orderItemOriginal->get('title')->value;

    if ($undo) {
      $message = t('@item refund has been undone (@refundedQuantity quantity restored)', [
        '@item'              => $itemTitle,
        '@refundedQuantity'  => $refundedQuantity,
        '@remainingQuantity' => $remainingQuantity,
      ]);
    } else {
      $message = t('@item has been refunded (@refundedQuantity quantity refunded, leaving @remainingQuantity)', [
        '@item'              => $itemTitle,
        '@refundedQuantity'  => $refundedQuantity,
        '@remainingQuantity' => $remainingQuantity,
      ]);
    }

    $commerceLog->generate($order, 'commerce_refund_order_item_status_template', ['comment' => $message])->save();
  }

  public function doLogMessageAdjustment($order, Adjustment $adjustment) {
    /** @var CommerceLogServiceProvider $commerceLog * */
    $commerceLog = \Drupal::entityTypeManager()->getStorage('commerce_log');

    $itemTitle = $adjustment->getLabel();
    $message = t('@item has been refunded (@amount @code)', [
      '@item'    => $itemTitle,
      '@amount'  => $adjustment['amount']->getAmount(),
      '@code'  => $adjustment['amount']->getCurrencyCode(),
    ]);

    $commerceLog->generate($order, 'commerce_refund_order_item_status_template', ['comment' => $message])->save();
  }

  public function stockControlledByCoreEvents() {
    if (!\Drupal::service('module_handler')->moduleExists('commerce_stock')) {
      return FALSE;
    }

    $config = \Drupal::configFactory()->get('commerce_stock.core_stock_events');
    return $config->get('core_stock_events_order_updates');
  }

  public function stockControlledByRefund() {
    // Not needed if "Adjust stock on order updates (after the order was completed)"

    if (!\Drupal::service('module_handler')->moduleExists('commerce_stock')) {
      return FALSE;
    }

    $config = \Drupal::configFactory()->get('commerce_stock.core_stock_events');
    if ($config->get('core_stock_events_order_updates')) {
      return FALSE;
    }

    // @todo make configurable for when not core_stock_events_order_updates
    return FALSE;
  }

  public function restoreStock($purchasedEntity, $refundedQuantity, $orderItemOriginal) {

    if (!$this->stockControlledByRefund()) {
      return;
    }

    /** @var \Drupal\commerce_stock\StockServiceManagerInterface $stockManager */
    $stockManager = \Drupal::service('commerce_stock.service_manager');
    /** @var \Drupal\commerce_stock\StockServiceInterface $stock_service */
    $stock_service = $stockManager->getService($purchasedEntity);
    if ($stock_service->getId() != 'always_in_stock') {
      $stockServiceManager = \Drupal::service('commerce_stock.service_manager');
      $stockServiceConfig = $stockServiceManager->getService($purchasedEntity)->getConfiguration();
      $current_user = \Drupal::currentUser();
      $current_store = \Drupal::service('commerce_store.current_store')->getStore();
      $context = new Context($current_user, $current_store);
      $locations = $stockServiceConfig->getAvailabilityLocations($context, $purchasedEntity);
      $unitPrice = $orderItemOriginal->getUnitPrice();
      $stockServiceManager->createTransaction($purchasedEntity, $locations[1]->getId(), '', $refundedQuantity, $unitPrice->getNumber(), $unitPrice->getCurrencyCode(), StockTransactionsInterface::STOCK_IN, []);
    }
  }

  public function updatePaymentForRefund(Payment $payment, $amount) {
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    } else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  public function getShipmentsForOrder(Order $order) {
    if (!$order->hasField('shipments')) {
      return NULL;
    }

    /** @var \Drupal\commerce_shipping\Entity\Shipment[] $shipments */
    $shipments = $order->get('shipments')->referencedEntities();
    $shipments = array_filter($shipments, function (ShipmentInterface $shipment) {
      return $shipment->getShippingMethodId() && $shipment->getAmount();
    });

    return $shipments;
  }

  /**
   * @param OrderItem $orderItem
   * @param array $itemsWithQuantity
   * key is order item ID
   * value is the quantity being refunded for that order item
   *
   * @return [type]
   */
  public function updateShipments($order, $itemsWithQuantity) {
    $shipments = $this->getShipmentsForOrder($order);

    if (!$shipments) {
      return;
    }

    /** @var \Drupal\commerce_shipping\Entity\Shipment $shipment */
    foreach ($shipments as $shipment) {
      /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state */
      $state = $shipment->getState();

      // We loop through all draft shipments, removing quantities from each until we've removed enough
      $stillToRefundQuantity = $itemsWithQuantity;

      if (!in_array($state->getId(), ['draft', 'processing'])) {
        continue;
      }

      foreach ($itemsWithQuantity as $orderItemId => $refundedQuantity) {
        /** @var \Drupal\commerce_shipping\ShipmentItem $item */
        foreach ($shipment->getItems() as $item) {
          if ($item->getOrderItemId() !== $orderItemId) {
            continue;
          }

          if ($stillToRefundQuantity[$orderItemId] <= 0) {
            continue;
          }

          if ($item->getQuantity() <= $stillToRefundQuantity[$orderItemId]) {
            // Refund the entire item
            $shipment->removeItem($item);
            $stillToRefundQuantity[$orderItemId] -= $item->getQuantity();
          } elseif ($item->getQuantity() > $stillToRefundQuantity[$orderItemId]) {
            // Reduce quantity
            $itemAsArray = $item->toArray();
            $itemAsArray['quantity'] = $item->getQuantity() - $stillToRefundQuantity[$orderItemId];
            $newItem = new ShipmentItem($itemAsArray);
            $shipment->removeItem($item);
            $shipment->addItem($newItem);

            $stillToRefundQuantity[$orderItemId] = 0;
          }
          $shipment->save();
        }
      }
    }
  }

  public function dispatchHooksAndEvents($order, $itemsWithQuantity, $undo = FALSE) {
    // Adding hook for other modules to do something after successfully refund an item
    \Drupal::moduleHandler()->invokeAll('commerce_refund_order_item_refund_success' . ($undo ? '_undo' : ''), [$itemsWithQuantity]);

    // Issue event for order having been refunded
    $event = new OrderRefundedEvent($order, $itemsWithQuantity);
    \Drupal::service('event_dispatcher')->dispatch('commerce_order.refund' . ($undo ? '_undo' : '') . '.order_item', $event);
  }

  public function refundPaymentNoGateway(PaymentInterface $payment, Price $amount = NULL, $itemsWithQuantity = NULL, $adjustments = NULL) {
    $this->refundFromGateway($itemsWithQuantity, $amount, $payment, $adjustments);
  }

  public function undoRefundPaymentNoGateway(PaymentInterface $payment, Price $amount = NULL, $itemsWithQuantity = NULL, $adjustments = NULL) {
    $this->undoRefundFromGateway($itemsWithQuantity, $amount, $payment, $adjustments);
  }
}
