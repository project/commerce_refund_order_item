<?php

/**
 * @file
 * Contains commerce_refund_order_item.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function commerce_refund_order_item_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
      // Main module help for the commerce_refund_order_item module.
    case 'help.page.commerce_refund_order_item':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Refund per order item') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_base_field_info().
 *
 * Source: https://drupal.stackexchange.com/questions/180885/using-hook-entity-base-field-info-for-adding-new-field
 * SourceL https://drupal.stackexchange.com/questions/214263/programmatically-add-a-taxonomy-term-as-entity-reference-in-a-custom-entity
 */
function commerce_refund_order_item_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];

  if ($entity_type->id() === 'commerce_order_item') {
    $fields['refunded'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Refunded?'))
      ->setSetting('on_label', t('Yes'))
      ->setSetting('off_label', t('No'))
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['refunded_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Refunded date'))
      ->setSettings([
        'datetime_type' => 'datetime'
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['refunded_amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Refunded amount'))
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['refunded_quantity'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Refunded quantity'))
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  return $fields;
}

/**
 * Implements hook_entity_operation().
 */
function commerce_refund_order_item_entity_operation_alter(array &$operations, EntityInterface $entity) {
  $entityTypeId = $entity->getEntityTypeId();
  if ($entityTypeId !== 'commerce_payment') {
    return;
  }

  $state = $entity->getState()->getId();
  if (in_array($state, ['refunded', 'partially_refunded'], TRUE)) {
    $editUrl = Url::fromRoute('commerce_refund_order_item.undo_refund_form', [
      'commerce_order' => $entity->getOrder()->id(),
      'commerce_payment' => $entity->id()
    ]);
    $operations['undo_refund'] = array(
      'title' => t('Undo refund'),
      'weight' => 20,
      'url' => $editUrl,
    );
  }
}
